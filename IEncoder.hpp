#pragma once

#include <cstdint>

class IEncoder
{
public:
  virtual ~IEncoder() = default;

  virtual void startEncoding( int width, int height ) = 0;
  virtual bool writeFrame( uint8_t const* y, int ystride, uint8_t const* u, int ustride, uint8_t const* v, int vstride ) = 0;
};
