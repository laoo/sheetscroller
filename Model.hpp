#pragma once

#include <atomic>
#include <mutex>
#include <thread>
#include <optional>
#include <queue>
#include <condition_variable>
#include <filesystem>
#include "IModel.hpp"
#include "IEncoder.hpp"
#include <AudioFile/AudioFile.h>
#include "Envelope.hpp"


struct ScrollingData;
class Project;
class VideoEncoder;

class Model : public IModel
{
public:
  Model( AudioFile<float> const& audioFile, Project const& project );
  ~Model() override;

  ScrollingData const& getData() override;
  std::shared_ptr<std::vector<float>> getBezierPoints() const override;

  void addKeyFrame( size_t before, float pos ) override;
  bool updateKeyFrame( size_t key, int frame ) override;
  void deleteKeyFrame( size_t key ) override;
  bool updateKeyOffset( size_t key, int offset ) override;

  void addEnvNode( int frame, float value ) override;
  void deleteEnvNode( size_t node ) override;
  void updateEnvNode( size_t node, int frame, float value ) override;

  void seek( float pos ) override;
  bool previewFades() const override;

  IEncoder * getEncoder() override;
  void stopEncoding();

  MappedPos map( int frame ) const;

  void resetLoop();

  bool playing() const;
  std::optional<float> isSeek() const;

  enum struct Mode
  {
    PREVIEW,
    RENDERING
  };

  Mode mode() const;
  void resetPreview();

private:
  void worker();
  void starRendering();

private:
  AudioFile<float> const& mAudioFile;
  int mFrames;
  mutable std::mutex mAccessMutex;
  mutable std::mutex mUpdateMutex;
  std::condition_variable mCondition;
  std::atomic<bool> mDoWork;
  std::unique_ptr<ScrollingData> mData;
  std::unique_ptr<Envelope> mEnvelope;
  std::thread mWorker;
  std::filesystem::path mOutputPath;
  int mVideoBitrate;
  int mAudioBitrate;
  int mFadeIn;
  int mFadeOut;
  int mCrossFade;
  bool mPreviewFades;
  std::unique_ptr<VideoEncoder> mEncoder;
  std::shared_ptr<std::vector<float>> mBezierPoints;

  bool mPlaying;
  Mode mMode;

  std::optional<float> mSeek;
};
