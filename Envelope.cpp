#include "Envelope.hpp"
#include <cassert>
#include <algorithm>
#include <iterator>
#include <vector>
#include <cmath>

#pragma warning( push )
#pragma warning( disable : 4267 )
#include <spline/src/spline.h>
#pragma warning( pop )

#include "Log.hpp"

#define M_PI 3.14159265358979323846f

namespace
{

template<typename F>
void bezierInterpolation( ScrollingData::Node const & n0, ScrollingData::Node const & n1, F const& f )
{
  double wv[4] = {
    n0.value,
    n0.value + n0.dy,
    n1.value - n1.dy,
    n1.value
  };

  double wf[4] = {
    (double)( n0.frame ),
    (double)( n0.frame + n0.dx ),
    (double)( n1.frame - n1.dx ),
    (double)( n1.frame )
  };

  int range = n1.frame - n0.frame;
  double r = (double)range;
  double rr = 1.0 / r;

  double ft = 0.0;
  for ( int i = 0; i <= range; ++i )
  {
    double lmin = (double)( n0.frame + i );
    double lmax = (double)( n0.frame + i + 1 );
    double t;
    for ( ;; )
    {
      t = Envelope::bezier( ft, wf );
      if ( t >= lmin )
        break;
      ft += rr;
    }
    if ( t > lmax )
    {
      ft -= rr;
      for ( double j = 2.0;; j += 1.0 )
      {
        t = Envelope::bezier( ft + rr / j, wf );
        if ( t < lmax )
        {
          ft += rr / j;
          break;
        }
      }
    }
    f( Envelope::bezier( ft, wv ) );
  }
}

}

double Envelope::bezier( double t, double w[4] )
{
  double t2 = t * t;
  double t3 = t2 * t;
  double mt = 1.0 - t;
  double mt2 = mt * mt;
  double mt3 = mt2 * mt;
  return w[0] * mt3 + 3 * w[1] * mt2 * t + 3 * w[2] * mt * t2 + w[3] * t3;
}


Envelope::Envelope( int frames, int fadeIn, int fadeOut, int crossFade, ScrollingData & data, std::shared_ptr<std::vector<float>> & bezierPoints ) : mKeyFrames{}, mEnvelope{}, mFrames{ frames }, mFadeIn{ fadeIn }, mFadeOut{ fadeOut }, mCrossFade{ crossFade }
{
  assert( data.nodes.size() >= 2 );
  assert( data.keyFrames.size() >= 2 );

  mEnvelope = computeFuntion( data.nodes, bezierPoints );

  mKeyFrames = data.keyFrames;
}

Envelope::~Envelope()
{
}

std::vector<double> Envelope::computeFuntion( std::vector<ScrollingData::Node> & nodes, std::shared_ptr<std::vector<float>> & bez )
{
  size_t points = nodes.back().frame + 1;
  std::vector<double> result;
  result.reserve( points );
  bez = std::make_shared<std::vector<float>>();
  bez->reserve( points );

  double sum = 0.0;
  for ( size_t i = 0; i < nodes.size() - 1; ++i )
  {
    bezierInterpolation( nodes[i], nodes[i + 1], [&]( double v )
    {
      bez->push_back( (float)v );
      sum += v;
      result.push_back( sum );
    } );
  }

  return result;
}


MappedPos Envelope::map( int idx ) const
{
  float off0 = 50000.0f;

  if ( idx <= 0 )
    return { off0, (float)mKeyFrames.front().offset, 0.0f };
  if ( idx >= mFrames - 1 )
    return { off0, (float)mKeyFrames.back().offset, 0.0f };

  float alpha = 1.0f;

  if ( idx < mFadeIn )
  {
    float mu = (float)idx / (float)mFadeIn;
    alpha = ( 1.0f - cosf( mu * M_PI ) ) / 2;
  }
  else if ( idx > mFrames - mFadeOut )
  {
    float mu = (float)( mFrames - idx ) / (float)mFadeOut;
    alpha = ( 1.0f - cosf( mu * M_PI * 0.5f ) );
  }

  auto it2 = std::find_if( mKeyFrames.cbegin(), mKeyFrames.cend(), [=]( ScrollingData::Key const& k )
  {
    return k.frame > idx;
  } );

  assert( it2 != mKeyFrames.cend() );

  std::vector<ScrollingData::Key>::const_iterator it0 = std::distance( mKeyFrames.cbegin(), it2 ) > 1 ? it2 - 2 : mKeyFrames.cend();
  std::vector<ScrollingData::Key>::const_iterator it1 = std::distance( mKeyFrames.cbegin(), it2 ) > 0 ? it2 - 1 : mKeyFrames.cend();

  if ( it2->offset >= it1->offset )
  {
    double s1 = mEnvelope[it1->frame];
    double s2 = mEnvelope[it2->frame];
    double d = s2 - s1;

    if ( d > 0.0 )
    {
      double m = mEnvelope[idx];
      double s = ( m - s1 ) / d;
      double l = std::lerp( (double)it1->offset, (double)it2->offset, s );

      L_TRACE << "(" << m << " - " << s1 << " ) / ( " << s2 << " - " << s1 << " ) = " << s << "       lerp( " << it1->offset << ", " << it2->offset << ", " << s << " ) = " << l;

      return { off0, (float)l, alpha };
    }
  }
  else if ( it2->frame - mCrossFade < idx )
  {
    off0 = (float)it2->offset;
    float mu = (float)( it2->frame - idx ) / mCrossFade;
    alpha = ( 1.0f - cosf( mu * M_PI ) ) / 2;
  }

  return { off0, (float)it1->offset, alpha };
}

