#pragma once

#include <Windows.h>
#pragma warning(push)
#pragma warning( disable: 4005 )
#include <d3d11.h>
#pragma warning(pop)
#include <atlbase.h>
#include <cstdint>
#include <array>
#include <memory>
#include <optional>
#include <utility>
#include <filesystem>
#include "Envelope.hpp"

class WinImgui;
class Project;
class IModel;
class IEncoder;

class WinPreviewRenderer
{
public:

  WinPreviewRenderer( Project const& project, int videoFrames );
  ~WinPreviewRenderer();
  void initialize( HWND hWnd );

  void renderPreview( IModel & model, MappedPos posAlpha );
  void renderGui( float pos, IModel & model );
  
  bool win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

private:
  struct CB
  {
    float srcOffX;
    float srcOffY;
    uint32_t dstOffX;
    uint32_t dstOffY;
    uint32_t dx;
    uint32_t dy;
    float alpha;
    float alpha2;
  };


private:
  void drawRuler( IModel & model, float pos );
  void drawKeyFrames( IModel & model, float pos, std::optional<size_t> & nodeUnderCursor );
  void drawOffset( IModel & model, std::optional<size_t> nodeUnderCursor );
  void drawEnvelopes( IModel & model, float pos );

  uint32_t previewWidth() const;
  uint32_t previewWidth( int div ) const;
  uint32_t previewHeight() const;
  uint32_t previewHeight( int div ) const;


private:
  HWND mHWnd;
  ATL::CComPtr<ID3D11Device>              mD3DDevice;
  ATL::CComPtr<ID3D11DeviceContext>       mImmediateContext;
  ATL::CComPtr<IDXGISwapChain>            mSwapChain;
  ATL::CComPtr<ID3D11ComputeShader>       mPass1CS;
  ATL::CComPtr<ID3D11ComputeShader>       mPass2CS;
  ATL::CComPtr<ID3D11Buffer>              mCBCB;
  ATL::CComPtr<ID3D11Texture2D>           mSource;
  ATL::CComPtr<ID3D11Texture2D>           mInter;
  ATL::CComPtr<ID3D11Texture2D>           mPreStagingY;
  ATL::CComPtr<ID3D11Texture2D>           mPreStagingU;
  ATL::CComPtr<ID3D11Texture2D>           mPreStagingV;
  ATL::CComPtr<ID3D11Texture2D>           mStagingY;
  ATL::CComPtr<ID3D11Texture2D>           mStagingU;
  ATL::CComPtr<ID3D11Texture2D>           mStagingV;
  ATL::CComPtr<ID3D11ShaderResourceView>  mSourceSRV;
  ATL::CComPtr<ID3D11ShaderResourceView>  mInterSRV;
  ATL::CComPtr<ID3D11UnorderedAccessView> mInterUAV;
  ATL::CComPtr<ID3D11UnorderedAccessView> mPreStagingYUAV;
  ATL::CComPtr<ID3D11UnorderedAccessView> mPreStagingUUAV;
  ATL::CComPtr<ID3D11UnorderedAccessView> mPreStagingVUAV;
  ATL::CComPtr<ID3D11UnorderedAccessView> mBackBufferUAV;
  ATL::CComPtr<ID3D11RenderTargetView>    mBackBufferRTV;
  CB mCB;

  std::unique_ptr<WinImgui> mImgui;
  std::shared_ptr<uint8_t[]> mSrcImage;
  uint32_t mSrcWidth;
  uint32_t mSrcHeight;
  uint32_t mWinWidth;
  uint32_t mWinHeight;
  uint32_t mPreviewWidth;
  uint32_t mPreviewHeight;
  uint32_t mSX;
  uint32_t mSY;
  uint32_t mD;
  int mVideoFrames;


};

