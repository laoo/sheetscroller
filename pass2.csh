R"(
Texture2D<unorm float4> src : register( t0 );
RWTexture2D<unorm float4> dst : register( u0 );

cbuffer cb : register( b0 )
{
  float2 _;
  uint2 dstOff;
  uint2 d;
};


[numthreads( THREADSX, THREADSY, 1 )]
void main( uint3 DT : SV_DispatchThreadID, uint3 G : SV_GroupID, uint3 GT : SV_GroupThreadID )
{
  dst[DT.xy + dstOff] = src[DT.xy * d];
}
)";
