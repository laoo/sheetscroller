#include "ScrollingData.hpp"

#include "Project.hpp"

ScrollingData::ScrollingData( Project const & project, int frames )
{
  if ( project.mKeyFrames.empty() )
  {
    keyFrames.push_back( { 0, 0 } );
    keyFrames.push_back( { 0, frames - 1 } );
  }
  else
  {
    if ( project.mKeyFrames.front().second != 0 )
    {
      keyFrames.push_back( { 0, 0 } );
    }

    for ( auto const& key : project.mKeyFrames )
    {
      int newFrame = ( std::min )( key.second, frames - 1 );
      if ( newFrame > keyFrames.back().frame )
      {
        keyFrames.push_back( { key.first, ( std::min )( key.second, frames - 1 ) } );
      }
    }

    if ( keyFrames.back().frame != frames - 1 )
    {
      keyFrames.push_back( { keyFrames.back().offset, frames - 1 } );
    }
  }

  if ( project.mNodes.empty() )
  {
    nodes.push_back( { 0.5f, 0, 0.0f, frames / 20 } );
    nodes.push_back( { 0.5f, frames - 1, 0.0f, frames / 20 } );
  }
  else
  {
    if ( project.mNodes.front().second != 0 )
    {
      nodes.push_back( { 0.0f, 0, 0.0f, frames / 20 } );
    }

    for ( auto const& key : project.mNodes )
    {
      nodes.push_back( { key.first, key.second, 0.0f, frames / 20 } );
    }

    if ( project.mNodes.back().second != 0 )
    {
      nodes.push_back( { 0.0f, frames - 1, 0.0f, frames / 20 } );
    }
  }
}

ScrollingData::~ScrollingData()
{
}
