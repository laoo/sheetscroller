#pragma once

#include <AudioFile/AudioFile.h>

#include <Windows.h>
#include <AudioClient.h>
#include <MMDeviceAPI.h>
#include <atlbase.h>

#include <vector>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <atomic>
#include <utility>
#include <filesystem>
#include <optional>

class WinAudioOut
{
public:

  WinAudioOut( AudioFile<float> const& audioFile );
  ~WinAudioOut();

  float sync( bool play, std::optional<float> pos = std::nullopt );

private:
  int fill( uint32_t cursor, uint32_t samples );

private:
  AudioFile<float> const& mAudioFile;
  uint32_t mBufferSize;
  WAVEFORMATEX * mMixFormat;
  uint32_t mCursor;
  uint32_t mLastCursor;

  ATL::CComPtr<IMMDeviceEnumerator> mDeviceEnumerator;
  ATL::CComPtr<IMMDevice> mDevice;
  ATL::CComPtr<IAudioClient> mAudioClient;
  ATL::CComPtr<IAudioRenderClient> mRenderClient;
  ATL::CComPtr<IAudioClock> mAudioClock;


};
