#include "WinAudioOut.hpp"
#include <stdexcept>
#include <cassert>
#include "Log.hpp"
#include "Ex.hpp"

WinAudioOut::WinAudioOut( AudioFile<float> const& audioFile ) : mAudioFile{ audioFile }, mBufferSize{}, mMixFormat{}, mCursor{}, mLastCursor{~0u}
{
  static constexpr uint32_t audioBufferLengthMs = 1000 * 2 / 60;

  L_DEBUG << "Sample length: " << mAudioFile.getLengthInSeconds() << " s";

  HRESULT hr;

  hr = CoInitializeEx( NULL, COINIT_MULTITHREADED );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = CoCreateInstance( __uuidof( MMDeviceEnumerator ), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS( &mDeviceEnumerator ) );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mDeviceEnumerator->GetDefaultAudioEndpoint( eRender, eMultimedia, &mDevice );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mDevice->Activate( __uuidof( IAudioClient ), CLSCTX_INPROC_SERVER, nullptr, reinterpret_cast<void **>( &mAudioClient ) );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mAudioClient->GetMixFormat( &mMixFormat );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mAudioClient->Initialize( AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_NOPERSIST, audioBufferLengthMs * 10000, 0, mMixFormat, nullptr );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mAudioClient->GetBufferSize( &mBufferSize );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mAudioClient->GetService( __uuidof( IAudioRenderClient ), reinterpret_cast<void **>( &mRenderClient ) );
  if ( FAILED( hr ) )
    throw Ex{};

  hr = mAudioClient->GetService( __uuidof( IAudioClock ), reinterpret_cast<void **>( &mAudioClock ) );
  if ( FAILED( hr ) )
    throw Ex{};

  mAudioClient->Start();
}

WinAudioOut::~WinAudioOut()
{
  mAudioClient->Stop();

  CoUninitialize();

  if ( mMixFormat )
  {
    CoTaskMemFree( mMixFormat );
    mMixFormat = nullptr;
  }
}

float WinAudioOut::sync( bool play, std::optional<float> pos )
{
  uint32_t samples = (uint32_t)mAudioFile.getNumSamplesPerChannel();

  if ( samples == 0 )
    return 0.0f;

  if ( pos.has_value() )
  {
    if ( *pos < 0.0f || *pos > 1.0f )
      throw Ex{} << "Bad position";

    uint32_t newCursor = (uint32_t)( *pos * (float)samples );

    if ( mLastCursor != newCursor )
    {
      mLastCursor = newCursor;
      mCursor = mLastCursor + fill( mLastCursor, samples );
    }
  }
  else if ( play )
  {
    mCursor += fill( mCursor, samples );
  }

  return (float)mCursor / (float)samples;
}

int WinAudioOut::fill( uint32_t cursor, uint32_t samples )
{
  HRESULT hr;

  uint32_t padding{};
  hr = mAudioClient->GetCurrentPadding( &padding );
  uint32_t framesAvailable = mBufferSize - padding;
  if ( framesAvailable > 0 )
  {
    BYTE *pData;
    hr = mRenderClient->GetBuffer( framesAvailable, &pData );
    if ( FAILED( hr ) )
      return 0;
    float* pfData = reinterpret_cast<float*>( pData );
    uint32_t ch = ( std::min )( ( uint32_t )mAudioFile.getNumChannels(), ( uint32_t )mMixFormat->nChannels );
    uint32_t limit = ( std::min )( framesAvailable, samples - cursor );
    for ( uint32_t i = 0; i < limit; ++i )
    {
      for ( uint32_t j = 0; j < ch; ++j )
      {
        pfData[i * mMixFormat->nChannels + j] = mAudioFile.samples[j][cursor + i];
      }
    }
    hr = mRenderClient->ReleaseBuffer( limit, 0 );
    if ( FAILED( hr ) )
      return 0;

    return limit;
  }

  return 0;
}


