#pragma once

#include <filesystem>
#include <cstdint>
#include <utility>

class Project
{
public:
  explicit Project( std::filesystem::path const& path );

  std::filesystem::path imagePath() const;
  std::filesystem::path audioPath() const;
  std::filesystem::path videoPath() const;
  int videoBitrate() const;
  int audioBitrate() const;
  int fadeIn() const;
  int fadeOut() const;
  int crossFade() const;

private:
  void addKeyFrame( int offset, int frame );
  void addNode( float value, int frame );

  static int parseTimecode( std::string const& tc );
private:
  std::filesystem::path mImagePath;
  std::filesystem::path mAudioPath;
  std::filesystem::path mVideoPath;
  int mVideoBitrate;
  int mAudioBitrate;
  int mFrames;
  int mFadeIn;
  int mFadeOut;
  int mCrossFade;

  std::vector<std::pair<int, int>> mKeyFrames;
  std::vector<std::pair<float,int>> mNodes;

  friend struct ScrollingData;
};
