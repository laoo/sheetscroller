#pragma once

#include <cstdint>
#include <filesystem>
#include <memory>
#include <functional>


#ifdef __cplusplus
extern "C" {
#endif

#define __STDC_CONSTANT_MACROS



#include <libavutil/avassert.h>
#include <libavutil/channel_layout.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>

#ifdef __cplusplus
}
#endif

#define STREAM_DURATION   3.0
#define STREAM_FRAME_RATE 60 /* 25 images/s */
#define SAMPLE_RATE 48000 /* 25 images/s */

#include "IEncoder.hpp"

class VideoEncoder : public IEncoder
{
public:
  VideoEncoder( std::filesystem::path const& path, int vbitrate, int abitrate );
  ~VideoEncoder() override;

  void setAutioCallback( std::function<std::pair<float, float>(size_t)> fun );

  void startEncoding( int width, int height ) override;
  bool writeFrame( uint8_t const* y, int ystride, uint8_t const* u, int ustride, uint8_t const* v, int vstride ) override;

private:
  void openVideo( int width, int height, int bitrate );
  void openAudio( int bitrate );
  int pushFrame( AVCodecContext *c, AVStream *st, AVFrame const* frame );
  static std::shared_ptr<AVFrame> allocVideoFrame( enum AVPixelFormat pix_fmt, int width, int height );
  static std::shared_ptr<AVFrame> allocAudioFrame( enum AVSampleFormat sample_fmt, uint64_t channel_layout, int sample_rate, int nb_samples );

private:
  struct StreamContext
  {
    AVStream *st;
    std::shared_ptr<AVCodecContext> encoder;

    /* pts of the next frame that will be generated */
    int64_t nextPts;

    std::shared_ptr<AVFrame> frame;
  };

private:
  std::filesystem::path const mPath;
  int const mVbitrate;
  int const mAbitrate;
  AVFormatContext *mFormatContext;
  AVCodec *mAudioCodec;
  AVCodec *mVideoCodec;
  std::shared_ptr<StreamContext> mAudioStream;
  std::shared_ptr<StreamContext> mVideoStream;

  std::function<std::pair<float, float>( size_t )> mAudioCallback;
};


