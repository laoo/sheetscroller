#pragma once

#include <vector>

class Project;

struct ScrollingData
{
  ScrollingData( Project const& project, int frames );
  ~ScrollingData();

  struct Key
  {
    int offset;
    int frame;
  };

  struct Node
  {
    float value;
    int frame;
    float dy;
    int dx;
  };

  std::vector<Key> keyFrames;
  std::vector<Node> nodes;

};
