#include "Model.hpp"
#include <chrono>
#include "Log.hpp"
#include <imgui/imgui.h>
#include "Project.hpp"
#include "VideoEncoder.hpp"
#include <Windows.h>

using namespace std::chrono_literals;

static constexpr size_t ENCODERS = 8;

Model::Model( AudioFile<float> const& audioFile, Project const& project ) : mAudioFile{ audioFile },  mFrames{ (int)( mAudioFile.getLengthInSeconds() * 60.0 ) },
  mAccessMutex{}, mUpdateMutex{}, mCondition{}, mDoWork{ true }, mData{}, mEnvelope{}, mWorker{ [this] { worker(); } }, mPlaying{}, mSeek{}, mMode{ Mode::PREVIEW }, mPreviewFades{ false }
{
  mFadeIn = project.fadeIn();
  mFadeOut = project.fadeOut();
  mCrossFade = project.crossFade();

  mData = std::make_unique<ScrollingData>( project, mFrames );

  mEnvelope = std::make_unique<Envelope>( mFrames, mFadeIn, mFadeOut, mCrossFade, *mData, mBezierPoints );

  mOutputPath = project.videoPath();
  mVideoBitrate = project.videoBitrate();
  mAudioBitrate = project.audioBitrate();
}

Model::~Model()
{
  mDoWork.store( false );
  mCondition.notify_one();
  mWorker.join();
}

ScrollingData const & Model::getData()
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  return *mData;
}

std::shared_ptr<std::vector<float>> Model::getBezierPoints() const
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  return mBezierPoints;
}

void Model::addKeyFrame( size_t before, float pos )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  if ( before >= mData->keyFrames.size() || before == 0 )
  {
    L_ERROR << "Bad key " << before;
    return;
  }

  int frame = (int)( mData->keyFrames.back().frame * pos );
  int offset = (int)mEnvelope->map( frame ).offset2;

  mData->keyFrames.insert( mData->keyFrames.begin() + before, { offset, frame } );
  mCondition.notify_one();
}

bool Model::updateKeyFrame( size_t key, int frame )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  if ( key >= mData->keyFrames.size() - 1 || key == 0 )
  {
    L_ERROR << "Bad key " << key;
    return false;
  }

  if ( frame > mData->keyFrames[key - 1].frame && frame < mData->keyFrames[key + 1].frame )
  {
    mData->keyFrames[key].frame = frame;
    mCondition.notify_one();
    return true;
  }
  else
  {
    return false;
  }

}

void Model::deleteKeyFrame( size_t key )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  if ( key >= mData->keyFrames.size() - 1 || key == 0 )
  {
    L_ERROR << "Bad key " << key;
    return;
  }

  mData->keyFrames.erase( mData->keyFrames.begin() + key );
  mCondition.notify_one();
}

bool Model::updateKeyOffset( size_t key, int offset )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };
  if ( key >= mData->keyFrames.size() )
  {
    L_ERROR << "Bad key " << key;
    return false;
  }

  mData->keyFrames[key].offset = offset;
  mCondition.notify_one();
  return true;
}

void Model::addEnvNode( int frame, float value )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };

  for ( auto it = mData->nodes.begin(); it != mData->nodes.end(); ++it )
  {
    auto it1 = it + 1;
    if ( it1 != mData->nodes.end() && it->frame + it->dx < frame && it1->frame - it1->dx > frame )
    {
      int dx1 = ( it->frame + it->dx + frame ) / 2;
      int dx2 = ( frame + it1->frame - it1->dx ) / 2;
      int dx = std::min( frame - dx1, dx2 - frame );

      mData->nodes.insert( it1, { value, frame, 0.0f, dx } );
      mCondition.notify_one();
      return;
    }
  }
}

void Model::deleteEnvNode( size_t node )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };

  size_t idx = node / 3;

  if ( idx >= mData->nodes.size() - 1 || idx == 0 )
  {
    L_ERROR << "Bad node " << idx;
    return;
  }

  if ( node % 3 != 1 )
    return;

  mData->nodes.erase( mData->nodes.begin() + idx );
  mCondition.notify_one();
}

void Model::updateEnvNode( size_t node, int frame, float value )
{
  std::unique_lock<std::mutex> lock{ mUpdateMutex };

  size_t idx = node / 3;

  if ( idx >= mData->nodes.size() )
  {
    L_ERROR << "Bad node " << idx;
    return;
  }

  float dy{};
  int dx{};
  switch ( node % 3 )
  {
  case 0:
    dx = mData->nodes[idx].frame - frame;
    dy = mData->nodes[idx].value - value;
    if ( idx > 0 && mData->nodes[idx].value + dy < 1.0f && mData->nodes[idx].value + dy >= 0.0f )
    {
      mData->nodes[idx].dy = dy;
    }
    if ( idx < mData->nodes.size() - 1 && mData->nodes[idx].frame + dx >= mData->nodes[idx+1].frame - mData->nodes[idx + 1].dx - 10 )
    {
      break;
    }
    mData->nodes[idx].dx = dx;
    break;
  case 1:
    mData->nodes[idx].value = value;
    if ( mData->nodes[idx].value + mData->nodes[idx].dy > 1.0f )
    {
      mData->nodes[idx].dy = 1.0f - mData->nodes[idx].value;
    }
    else if ( mData->nodes[idx].value - mData->nodes[idx].dy > 1.0f )
    {
      mData->nodes[idx].dy = mData->nodes[idx].value - 1.0f;
    }
    else if ( mData->nodes[idx].value + mData->nodes[idx].dy < 0.0f )
    {
      mData->nodes[idx].dy = -mData->nodes[idx].value;
    }
    else if ( mData->nodes[idx].value - mData->nodes[idx].dy < 0.0f )
    {
      mData->nodes[idx].dy = mData->nodes[idx].value;
    }
    if ( idx < mData->nodes.size() - 1  && frame + mData->nodes[idx].dx >= mData->nodes[idx + 1].frame - mData->nodes[idx + 1].dx - 10 )
    {
      break;
    }
    if ( idx > 0 && frame - mData->nodes[idx].dx < mData->nodes[idx - 1].frame + mData->nodes[idx - 1].dx + 10 )
    {
      break;
    }
    mData->nodes[idx].frame = frame;
    break;
  case 2:
    dx = frame - mData->nodes[idx].frame;
    dy = value - mData->nodes[idx].value;
    if ( idx < mData->nodes.size() - 1 && mData->nodes[idx].value - dy < 1.0f && mData->nodes[idx].value - dy >= 0.0f )
    {
      mData->nodes[idx].dy = dy;
    }
    if ( idx > 0 && mData->nodes[idx].frame - dx < mData->nodes[idx - 1].frame + mData->nodes[idx - 1].dx + 10 )
    {
      break;
    }
    mData->nodes[idx].dx = dx;
    break;
  default:
    break;
  }

  mCondition.notify_one();
}

void Model::seek( float pos )
{
  mSeek = pos;
}

bool Model::previewFades() const
{
  return mPreviewFades;
}

void Model::stopEncoding()
{
  mEncoder.reset();
}

MappedPos Model::map( int frame ) const
{
  std::unique_lock<std::mutex> lock{ mAccessMutex };
  if ( mEnvelope )
    return mEnvelope->map( frame );
  else
    return { 50000.0f, 50000.0f, 0.0f };
}

void Model::resetLoop()
{
  if ( ImGui::IsKeyPressed( VK_SPACE, false ) )
  {
    mPlaying = !mPlaying;
  }

  if ( ImGui::IsKeyPressed( 'R', false ) )
  {
    switch ( mMode )
    {
    case Mode::PREVIEW:
      starRendering();
      break;
    case Mode::RENDERING:
      resetPreview();
      break;
    }
  }

  if ( ImGui::IsKeyPressed( 'F', false ) )
  {
    mPreviewFades = !mPreviewFades;
  }

  mSeek.reset();
}

bool Model::playing() const
{
  return mPlaying;
}

std::optional<float> Model::isSeek() const
{
  return mSeek;
}

IEncoder * Model::getEncoder()
{
  return mEncoder.get();
}

Model::Mode Model::mode() const
{
  return mMode;
}

void Model::resetPreview()
{
  mMode = Mode::PREVIEW;
  stopEncoding();
}

void Model::worker()
{
  while ( mDoWork.load() )
  {
    std::unique_ptr<ScrollingData> dataCopy;
    {
      std::unique_lock<std::mutex> lock{ mUpdateMutex };
      mCondition.wait( lock );
      dataCopy.reset( new ScrollingData( *mData ) );
    }
    std::shared_ptr<std::vector<float>> bezierPoints;
    auto newEnvelope = std::make_unique<Envelope>( mFrames, mFadeIn, mFadeOut, mCrossFade, *dataCopy, bezierPoints );
    {
      std::unique_lock<std::mutex> lock{ mAccessMutex };
      std::swap( mEnvelope, newEnvelope );
    }
    {
      std::unique_lock<std::mutex> lock{ mUpdateMutex };
      mBezierPoints = std::move( bezierPoints );
    }
  }
}

void Model::starRendering()
{
  mMode = Mode::RENDERING;
  mEncoder.reset( new VideoEncoder( mOutputPath, mVideoBitrate, mAudioBitrate ) );
  mEncoder->setAutioCallback( [&]( size_t idx )
  {
    if ( idx < mAudioFile.samples[0].size() )
    {
      return std::make_pair( mAudioFile.samples[0][idx], mAudioFile.samples[1][idx] );
    }
    else
    {
      return std::make_pair( 0.0f, 0.0f );
    }
  } );
}
