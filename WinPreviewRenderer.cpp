#include "WinPreviewRenderer.hpp"
#include "WinImgui.hpp"
#include <d3dcompiler.h>

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cassert>
#include <fstream>
#include <bit>

#include <stdexcept>
#include <chrono>
#include "Ex.hpp"
#include "Log.hpp"
#include "Project.hpp"
#include "IModel.hpp"
#include "IEncoder.hpp"
#include "ScrollingData.hpp"
#include <lodepng/lodepng.h>

//namespace std
//{
//template<class T>
//constexpr int countr_zero( T x ) noexcept
//{
//  return __builtin_ctz( x );
//}
//}

#define V_THROW(x) { HRESULT hr_ = (x); if( FAILED( hr_ ) ) { throw Ex{} << "DXError"; } }
using namespace std::string_literals;

WinPreviewRenderer::WinPreviewRenderer( Project const& project, int videoFrames ) : mHWnd{}, mCB{}, mImgui{}, mVideoFrames{ videoFrames }
{
  uint8_t * img;

  if ( auto status = lodepng_decode32_file( &img, &mSrcWidth, &mSrcHeight, project.imagePath().string().c_str() ) )
  {
    throw Ex{} << "lodepng:"s << lodepng_error_text( status ) << " while loading file " << project.imagePath().string();
  }

  int h = ( ( mSrcHeight / 16384 ) + 1 ) * 16384;
  int s = mSrcWidth * h * 4;

  mSrcImage = std::shared_ptr<uint8_t[]>{ new uint8_t[mSrcWidth * h * 4] };

  std::memset( mSrcImage.get(), 0, s );
  std::memcpy( mSrcImage.get(), img, mSrcWidth * mSrcHeight * 4 );

  ::free( (void*)img );
}

void WinPreviewRenderer::initialize( HWND hWnd )
{

  mHWnd = hWnd;

  typedef HRESULT( WINAPI * LPD3D11CREATEDEVICE )( IDXGIAdapter*, D3D_DRIVER_TYPE, HMODULE, UINT32, CONST D3D_FEATURE_LEVEL*, UINT, UINT32, ID3D11Device**, D3D_FEATURE_LEVEL*, ID3D11DeviceContext** );
  static LPD3D11CREATEDEVICE  s_DynamicD3D11CreateDevice = nullptr;
  HMODULE hModD3D11 = ::LoadLibrary( L"d3d11.dll" );
  if ( hModD3D11 == nullptr )
    throw std::runtime_error{ "DXError" };

  s_DynamicD3D11CreateDevice = (LPD3D11CREATEDEVICE)GetProcAddress( hModD3D11, "D3D11CreateDevice" );


  D3D_FEATURE_LEVEL  featureLevelsRequested = D3D_FEATURE_LEVEL_11_0;
  UINT               numFeatureLevelsRequested = 1;
  D3D_FEATURE_LEVEL  featureLevelsSupported;

  HRESULT hr = s_DynamicD3D11CreateDevice( nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr,
#ifndef NDEBUG
    D3D11_CREATE_DEVICE_DEBUG,
#else
    0,
#endif
    & featureLevelsRequested, numFeatureLevelsRequested, D3D11_SDK_VERSION, &mD3DDevice, &featureLevelsSupported, &mImmediateContext );

  V_THROW( hr );

  DXGI_SWAP_CHAIN_DESC sd{ { 0, 0, { 60, 1 }, DXGI_FORMAT_R8G8B8A8_UNORM }, { 1, 0 }, DXGI_USAGE_UNORDERED_ACCESS | DXGI_USAGE_RENDER_TARGET_OUTPUT, 2, mHWnd, TRUE, DXGI_SWAP_EFFECT_DISCARD, 0 };
  ATL::CComPtr<IDXGIDevice> pDXGIDevice;
  V_THROW( mD3DDevice->QueryInterface( __uuidof( IDXGIDevice ), (void **)&pDXGIDevice ) );
  ATL::CComPtr<IDXGIAdapter> pDXGIAdapter;
  V_THROW( pDXGIDevice->GetParent( __uuidof( IDXGIAdapter ), (void **)&pDXGIAdapter ) );
  ATL::CComPtr<IDXGIFactory> pIDXGIFactory;
  V_THROW( pDXGIAdapter->GetParent( __uuidof( IDXGIFactory ), (void **)&pIDXGIFactory ) );
  ATL::CComPtr<IDXGISwapChain> pSwapChain;
  V_THROW( pIDXGIFactory->CreateSwapChain( mD3DDevice, &sd, &pSwapChain ) );
  mSwapChain = std::move( pSwapChain );

  D3D11_BUFFER_DESC bd{ sizeof( mCB ), D3D11_USAGE_DEFAULT, D3D11_BIND_CONSTANT_BUFFER };
  V_THROW( mD3DDevice->CreateBuffer( &bd, NULL, &mCBCB ) );

  {
    uint32_t arraySize = ( mSrcHeight / 16384 ) + 1;
    D3D11_TEXTURE2D_DESC descsrc{ mSrcWidth, 16384, 1, arraySize, DXGI_FORMAT_R8G8B8A8_UNORM, { 1, 0 }, D3D11_USAGE_IMMUTABLE, D3D11_BIND_SHADER_RESOURCE };
    std::vector<D3D11_SUBRESOURCE_DATA> sda;
    for ( size_t i = 0; i < arraySize; ++i )
    {
      sda.push_back( D3D11_SUBRESOURCE_DATA{ mSrcImage.get() + mSrcWidth * 4 * 16384 * i, mSrcWidth * 4, 0 } );
    }
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, sda.data(), &mSource ) );
    V_THROW( mD3DDevice->CreateShaderResourceView( mSource, NULL, &mSourceSRV.p ) );
  }

  mSrcImage.reset();

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth(), previewHeight(), 1, 1, DXGI_FORMAT_R8G8B8A8_UNORM, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mInter ) );
    V_THROW( mD3DDevice->CreateShaderResourceView( mInter, NULL, &mInterSRV.p ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( mInter, NULL, &mInterUAV.p ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth(), previewHeight(), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_UNORDERED_ACCESS };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mPreStagingY ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( mPreStagingY, NULL, &mPreStagingYUAV.p ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth(), previewHeight(), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_READ };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mStagingY ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth( 2 ), previewHeight( 2 ), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_UNORDERED_ACCESS };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mPreStagingU ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( mPreStagingU, NULL, &mPreStagingUUAV.p ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth( 2 ), previewHeight( 2 ), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_READ };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mStagingU ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth( 2 ), previewHeight( 2 ), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_UNORDERED_ACCESS };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mPreStagingV ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( mPreStagingV, NULL, &mPreStagingVUAV.p ) );
  }

  {
    D3D11_TEXTURE2D_DESC descsrc{ previewWidth( 2 ), previewHeight( 2 ), 1, 1, DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_READ };
    V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mStagingV ) );
  }

  {

    int sx = 1 << std::countr_zero( previewWidth() );
    int sy = 1 << std::countr_zero( previewHeight() );

    ATL::CComPtr<ID3D10Blob> csBlob;
    ATL::CComPtr<ID3D10Blob> csErrBlob;

    char computeShader[] =
#include "pass1.csh"

      D3DCompile( computeShader, strlen( computeShader ), "ComputeShader", nullptr, NULL, "main", "cs_5_0", 0, 0, &csBlob.p, &csErrBlob.p );
    if ( !csBlob ) // NB: Pass ID3D10Blob* pErrorBlob to D3DCompile() to get error showing in (const char*)pErrorBlob->GetBufferPointer(). Make sure to Release() the blob!
    {
      L_ERROR << (char*)csErrBlob->GetBufferPointer();
      throw Ex{} << "cs blob error";
    }

    mPass1CS.Release();
    V_THROW( mD3DDevice->CreateComputeShader( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), nullptr, &mPass1CS ) );

    ATL::CComPtr<ID3D10Blob> csDisAsm;
    V_THROW( D3DDisassemble( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), D3D_DISASM_ENABLE_INSTRUCTION_OFFSET | D3D_DISASM_INSTRUCTION_ONLY, "Waldek", &csDisAsm.p ) );
    L_DEBUG << (char*)csDisAsm->GetBufferPointer();
  }

  mImgui.reset( new WinImgui{ mHWnd, mD3DDevice, mImmediateContext } );
}

bool WinPreviewRenderer::win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
  return mImgui->win32_WndProcHandler( hWnd, msg, wParam, lParam );
}

WinPreviewRenderer::~WinPreviewRenderer()
{
  ImGui::DestroyContext();
}

void WinPreviewRenderer::renderPreview( IModel & model, MappedPos posAlpha )
{
  auto [off1, off2, alpha] = posAlpha;

  float phase = std::sinf( ( std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::high_resolution_clock::now().time_since_epoch() ).count() % 1000 ) / 1000.0f * 2.0f * 3.14159f ) * 0.5f + 0.5f;

  RECT r;
  ::GetClientRect( mHWnd, &r );

  if ( mWinHeight != ( r.bottom - r.top ) || ( mWinWidth != r.right - r.left ) )
  {
    mWinHeight = r.bottom - r.top;
    mWinWidth = r.right - r.left;

    if ( mWinHeight == 0 || mWinWidth == 0 )
    {
      return;
    }

    mBackBufferRTV.Release();
    mBackBufferUAV.Release();

    mSwapChain->ResizeBuffers( 0, mWinWidth, mWinHeight, DXGI_FORMAT_UNKNOWN, 0 );

    ATL::CComPtr<ID3D11Texture2D> backBuffer;
    V_THROW( mSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&backBuffer.p ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( backBuffer, nullptr, &mBackBufferUAV.p ) );
    V_THROW( mD3DDevice->CreateRenderTargetView( backBuffer, nullptr, &mBackBufferRTV.p ) );

    uint32_t dx = previewWidth() / mWinWidth + 1;
    uint32_t dy = previewHeight() / mWinHeight + 1;
    mD = ( std::max )( ( std::max )( dx, dy ), 1u );

    mPreviewWidth =  previewWidth() / mD;
    mPreviewHeight = previewHeight() / mD;

    int sx = 1 << std::countr_zero( mPreviewWidth );
    int sy = 1 << std::countr_zero( mPreviewHeight );

    if ( sx != mSX || sy != mSY )
    {
      mSX = sx;
      mSY = sy;

      char THREADSX[10];
      char THREADSY[10];

      sprintf( THREADSX, "%d", mSX );
      sprintf( THREADSY, "%d", mSY );

      D3D_SHADER_MACRO shaderMacros[] ={
        "THREADSX", THREADSX,
        "THREADSY", THREADSY,
        NULL, NULL
      };

      ATL::CComPtr<ID3D10Blob> csBlob;
      ATL::CComPtr<ID3D10Blob> csErrBlob;

      char computeShader[] =
#include "pass2.csh"

        D3DCompile( computeShader, strlen( computeShader ), "ComputeShader", shaderMacros, NULL, "main", "cs_5_0", 0, 0, &csBlob.p, &csErrBlob.p );
      if ( !csBlob ) // NB: Pass ID3D10Blob* pErrorBlob to D3DCompile() to get error showing in (const char*)pErrorBlob->GetBufferPointer(). Make sure to Release() the blob!
      {
        L_ERROR << (char*)csErrBlob->GetBufferPointer();
        throw Ex{} << "cs blob error";
      }

      mPass2CS.Release();
      V_THROW( mD3DDevice->CreateComputeShader( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), nullptr, &mPass2CS ) );

      ATL::CComPtr<ID3D10Blob> csDisAsm;
      V_THROW( D3DDisassemble( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), D3D_DISASM_ENABLE_INSTRUCTION_OFFSET | D3D_DISASM_INSTRUCTION_ONLY, "Waldek", &csDisAsm.p ) );
      L_DEBUG << (char*)csDisAsm->GetBufferPointer();
    }
  }

  ImGuiIO& io = ImGui::GetIO();

  mCB ={
    off1,
    off2,
    ( mWinWidth - mPreviewWidth ) / 2,
    ( mWinHeight - mPreviewHeight ) / 2,
    mD, mD,
    alpha,
    model.previewFades() ? alpha : 1.0f
  };

  ID3D11ShaderResourceView * nullsrv{};

  mImmediateContext->UpdateSubresource( mCBCB, 0, NULL, &mCB, 0, 0 );
  mImmediateContext->CSSetConstantBuffers( 0, 1, &mCBCB.p );
  mImmediateContext->CSSetShaderResources( 0, 1, &mSourceSRV.p );
  std::array<ID3D11UnorderedAccessView *, 4> uavs{ mInterUAV.p, mPreStagingYUAV.p, mPreStagingUUAV.p, mPreStagingVUAV.p };
  mImmediateContext->CSSetUnorderedAccessViews( 0, 4, uavs.data(), nullptr );
  mImmediateContext->CSSetShader( mPass1CS, nullptr, 0 );
  mImmediateContext->Dispatch( previewWidth( 256 ), previewHeight( 16 ), 1 );

  uavs[0] = mBackBufferUAV.p;
  uavs[1] = nullptr;
  uavs[2] = nullptr;
  uavs[3] = nullptr;
  mImmediateContext->CSSetUnorderedAccessViews( 0, 4, uavs.data(), nullptr );
  mImmediateContext->CSSetShaderResources( 0, 1, &mInterSRV.p );
  mImmediateContext->CSSetShader( mPass2CS, nullptr, 0 );
  UINT v[4]={ 255, 255, 255, 255 };
  mImmediateContext->ClearUnorderedAccessViewUint( mBackBufferUAV, v );
  mImmediateContext->Dispatch( mPreviewWidth / mSX, mPreviewHeight / mSY, 1 );
  mImmediateContext->CSSetShaderResources( 0, 1, &nullsrv );

  uavs[0] = nullptr;
  mImmediateContext->CSSetUnorderedAccessViews( 0, 4, uavs.data(), nullptr );

  if ( auto pEncoder = model.getEncoder() )
  {
    mImmediateContext->CopyResource( mStagingY, mPreStagingY );
    mImmediateContext->CopyResource( mStagingU, mPreStagingU );
    mImmediateContext->CopyResource( mStagingV, mPreStagingV );

    D3D11_MAPPED_SUBRESOURCE resY, resU, resV;
    mImmediateContext->Map( mStagingY, 0, D3D11_MAP_READ, 0, &resY );
    mImmediateContext->Map( mStagingU, 0, D3D11_MAP_READ, 0, &resU );
    mImmediateContext->Map( mStagingV, 0, D3D11_MAP_READ, 0, &resV );

    if ( !pEncoder->writeFrame( (uint8_t const*)resY.pData, resY.RowPitch, (uint8_t const*)resU.pData, resU.RowPitch, (uint8_t const*)resV.pData, resV.RowPitch ) )
    {
      pEncoder->startEncoding( previewWidth(), previewHeight() );
      bool res = pEncoder->writeFrame( (uint8_t const*)resY.pData, resY.RowPitch, (uint8_t const*)resU.pData, resU.RowPitch, (uint8_t const*)resV.pData, resV.RowPitch );
      assert( res );
    }
    
    mImmediateContext->Unmap( mStagingY, 0 );
    mImmediateContext->Unmap( mStagingU, 0 );
    mImmediateContext->Unmap( mStagingV, 0 );
  }
}

void WinPreviewRenderer::drawRuler( IModel & model, float pos )
{
  ImGuiIO& io = ImGui::GetIO();

  ImVec2 c0 = ImGui::GetCursorScreenPos();
  ImVec2 cs = ImVec2{ ImGui::GetContentRegionAvail().x, 30 };
  ImVec2 c1{ c0.x + cs.x, c0.y + cs.y };
  ImDrawList* draw_list = ImGui::GetWindowDrawList();
  draw_list->AddRectFilled( c0, c1, IM_COL32( 0, 100, 100, 128 ) );

  ImGui::InvisibleButton( "Ruler", cs, ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight );
  const bool is_hovered = ImGui::IsItemHovered(); // Hovered
  const bool is_active = ImGui::IsItemActive();   // Held

  if ( is_hovered )
  {
    int frames = (int)( pos * mVideoFrames );
    int minutes{}, seconds{};

    if ( frames >= 60 * 60 )
    {
      minutes = frames / ( 60 * 60 );
      frames -= minutes * 60 * 60;
    }
    if ( frames >= 60 )
    {
      seconds = frames / 60;
      frames -= seconds * 60;
    }

    char timecode[50];
    sprintf( timecode, "%02d:%02d:%02d", minutes, seconds, frames );

    ImGui::SetNextWindowBgAlpha( 0.5f );
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos( ImGui::GetFontSize() * 35.0f );
    ImGui::TextUnformatted( timecode );
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();

  }


  static std::optional<int> sScrubPos;

  draw_list->AddLine( ImVec2( c0.x + cs.x * pos, c0.y ), ImVec2( c0.x + cs.x * pos, c0.y + cs.y ), IM_COL32( 255, 255, 255, 255 ), 2.0f );
  if ( is_active )
  {
    int mousePos = (int)( io.MousePos.x - c0.x );
    if ( sScrubPos && *sScrubPos == mousePos )
      return;

    sScrubPos = mousePos;

    model.seek( std::clamp( (float)mousePos / cs.x, 0.0f, 1.0f ) );
  }
  else
  {
    sScrubPos.reset();
  }
}

void WinPreviewRenderer::drawEnvelopes( IModel & model, float pos )
{
  ImGuiIO& io = ImGui::GetIO();
  auto const& data = model.getData();

  ImVec2 c0 = ImGui::GetCursorScreenPos();
  ImVec2 cs = ImGui::GetContentRegionAvail();
  ImVec2 c1{ c0.x + cs.x, c0.y + cs.y };
  ImVec2 mousePos{ io.MousePos.x - c0.x, io.MousePos.y - c0.y };

  ImDrawList* draw_list = ImGui::GetWindowDrawList();
  draw_list->AddRectFilled( c0, c1, IM_COL32( 25, 25, 25, 128 ) );

  ImGui::InvisibleButton( "Envelopes", cs, ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight );
  const bool is_hovered = ImGui::IsItemHovered(); // Hovered
  const bool is_active = ImGui::IsItemActive();   // Held

  draw_list->AddLine( ImVec2( c0.x + cs.x * pos, c0.y ), ImVec2( c0.x + cs.x * pos, c0.y + cs.y ), IM_COL32( 128, 128, 128, 128 ), 0.5f );

  struct Node
  {
    ImVec2 pos;
    bool hovered;
  };

  std::vector<Node> nodes;
  std::optional<size_t> hoveredPoint{};
  int maxFrame = data.keyFrames.back().frame;

  auto app = [&]( float x, float y )
  {
    Node node{};
    node.pos = ImVec2{ x, y };
    node.hovered = std::hypot( mousePos.x - node.pos.x, mousePos.y - node.pos.y ) < 5.0f;
    if ( node.hovered )
      hoveredPoint = nodes.size();

    nodes.push_back( node );
  };


  for ( size_t i = 0; i < data.nodes.size(); ++i )
  {
    app( ( data.nodes[i].frame - data.nodes[i].dx ) * cs.x / maxFrame, cs.y * ( 1.0f - ( data.nodes[i].value - data.nodes[i].dy ) ) );
    app( data.nodes[i].frame * cs.x / maxFrame, cs.y * ( 1.0f - data.nodes[i].value ) );
    app( ( data.nodes[i].frame + data.nodes[i].dx ) * cs.x / maxFrame, cs.y * ( 1.0f - ( data.nodes[i].value + data.nodes[i].dy ) ) );
  }

  static std::optional<size_t> dragNode;
  if ( hoveredPoint.has_value() && ImGui::IsMouseClicked( ImGuiButtonFlags_MouseButtonLeft ) )
  {
    dragNode = hoveredPoint;
  }

  if ( dragNode.has_value() )
  {
    if ( ImGui::IsMouseReleased( ImGuiButtonFlags_MouseButtonLeft ) )
    {
      dragNode.reset();
    }
    else
    {
      int newFrame{};

      if ( *dragNode > 1 && *dragNode < nodes.size() - 2 )
      {
        float fpos = std::clamp( mousePos.x, nodes[*dragNode - 1].pos.x + cs.x / 100, nodes[*dragNode + 1].pos.x - cs.x / 100 );
        newFrame = (int)( fpos * maxFrame / cs.x );
      }
      else if ( *dragNode <= 1 )
      {
        newFrame = data.nodes.front().frame;
      }
      else
      {
        newFrame = data.nodes.back().frame;
      }

      model.updateEnvNode( *dragNode, newFrame, std::clamp( ( cs.y - mousePos.y ) / cs.y, 0.0f, 1.0f ) );
    }
  }

  for ( size_t i = 1; i < nodes.size() - 2; ++i )
  {
    auto const& n0 = nodes[i];
    auto const& n1 = nodes[i + 1];

    constexpr ImU32 color = IM_COL32( 255, 255, 200, 255 );
    constexpr float thickness = 1.0f;

    draw_list->AddLine( ImVec2{ c0.x + n0.pos.x, c0.y + n0.pos.y }, ImVec2{ c0.x + n1.pos.x, c0.y + n1.pos.y }, color, thickness );
  }

  auto bezierPoints = model.getBezierPoints();

  float point = c0.y + cs.y * ( 1.0f - bezierPoints->at( 0 ) );
  for ( size_t i = 5; i < bezierPoints->size(); i += 5 )
  {
    float next = c0.y + cs.y * ( 1.0f - bezierPoints->at( i ) );
    draw_list->AddLine( ImVec2{ c0.x + ( i - 5 ) * cs.x / maxFrame, point }, ImVec2{ c0.x + ( i ) * cs.x / maxFrame, next }, IM_COL32( 255, 0, 0, 128 ), 1.0 );
    point = next;
  }

  for ( size_t i = 1; i < nodes.size() - 1; ++i )
  {
    auto const& node = nodes[i];

    ImU32 color = node.hovered ? IM_COL32( 100, 255, 100, 255 ) : IM_COL32( 255, 255, 200, 255 );
    float radius = node.hovered ? 7.0f : 5.0f;

    draw_list->AddCircleFilled( ImVec2{ c0.x + node.pos.x, c0.y + node.pos.y }, radius, color );
  }


  static std::optional<ImVec2> menuLine;
  static std::optional<ImVec2> menuPoint;
  static std::optional<size_t> menuPointId;

  ImVec2 dragDelta = ImGui::GetMouseDragDelta( ImGuiMouseButton_Right );
  if ( ImGui::IsMouseReleased( ImGuiMouseButton_Right ) && dragDelta.x == 0.0f && dragDelta.y == 0.0f )
  {
    if ( hoveredPoint.has_value() )
    {
      menuPoint = mousePos;
      menuPointId = *hoveredPoint;
      ImGui::OpenPopupOnItemClick( "contextEnvDel" );
    }
    else
    {
      menuLine = mousePos;
      ImGui::OpenPopupOnItemClick( "contextEnvAdd" );
    }
  }

  ImGui::PushStyleVar( ImGuiStyleVar_WindowPadding, ImVec2( 4, 4 ) );

  if ( menuLine.has_value() )
  {
    if ( ImGui::BeginPopup( "contextEnvAdd" ) )
    {
      if ( ImGui::MenuItem( "Add node", NULL, false, true ) )
      {
        model.addEnvNode( std::clamp( (int)( menuLine->x * maxFrame / cs.x ), 1, maxFrame - 1 ), std::clamp( ( cs.y - menuLine->y ) / cs.y, 0.0f, 1.0f ) );
      }
      ImGui::EndPopup();
    }

    if ( !ImGui::IsPopupOpen( "contextEnvAdd" ) )
    {
      menuLine.reset();
    }
  }
  else if ( menuPoint.has_value() )
  {
    if ( ImGui::BeginPopup( "contextEnvDel" ) )
    {
      if ( ImGui::MenuItem( "Delete node", NULL, false, true ) )
      {
        model.deleteEnvNode( *menuPointId );
      }
      ImGui::EndPopup();
    }

    if ( !ImGui::IsPopupOpen( "contextEnvDel" ) )
    {
      menuPoint.reset();
      menuPointId.reset();
    }
  }

  ImGui::PopStyleVar();
}

uint32_t WinPreviewRenderer::previewWidth() const
{
  switch ( mSrcWidth )
  {
  case 1920:
    return 1920;
  case 3840:
    return 3840;
  default:
    throw Ex{} << "Unsuported width " << mSrcWidth;
  }
}

uint32_t WinPreviewRenderer::previewWidth( int div ) const
{
  return (uint32_t)std::ceil( (double)previewWidth() / (double)div );
}

uint32_t WinPreviewRenderer::previewHeight() const
{
  switch ( mSrcWidth )
  {
  case 1920:
    return 1080;
  case 3840:
    return 2160;
  default:
    throw Ex{} << "Unsuported width " << mSrcWidth;
  }
}

uint32_t WinPreviewRenderer::previewHeight( int div ) const
{
  return (uint32_t)std::ceil( (double)previewHeight() / (double)div );
}

void WinPreviewRenderer::drawKeyFrames( IModel & model, float pos, std::optional<size_t> & nodeUnderCursor )
{
  static constexpr float halfWidth = 5.0f;

  ImGuiIO& io = ImGui::GetIO();
  auto const& data = model.getData();

  ImVec2 c0 = ImGui::GetCursorScreenPos();
  ImVec2 cs = ImVec2{ ImGui::GetContentRegionAvail().x, 25 };
  ImVec2 c1{ c0.x + cs.x, c0.y + cs.y };
  auto mousePos = std::clamp( io.MousePos.x - c0.x, 0.0f, cs.x );

  ImDrawList* draw_list = ImGui::GetWindowDrawList();
  draw_list->AddRectFilled( c0, c1, IM_COL32( 0, 0, 100, 128 ) );

  ImGui::InvisibleButton( "KeyFrames", cs, ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight );
  const bool is_hovered = ImGui::IsItemHovered(); // Hovered
  const bool is_active = ImGui::IsItemActive();   // Held

  draw_list->AddLine( ImVec2( c0.x + cs.x * pos, c0.y ), ImVec2( c0.x + cs.x * pos, c0.y + cs.y ), IM_COL32( 128, 128, 128, 128 ), 0.5f );

  int maxFrame = data.keyFrames.back().frame;
  std::vector<std::pair<float, bool>> nodeList;
  static std::optional<size_t> hoveredNode;
  static float draggedPos;
  for ( size_t i = 0; i < data.keyFrames.size(); ++i )
  {
    auto const& k = data.keyFrames[i];
    float posx = k.frame * cs.x / maxFrame;
    ImVec2 r0{ c0.x + posx - halfWidth, c0.y };
    ImVec2 r1{ c0.x + posx + halfWidth, c1.y };
    bool hovered = is_hovered && ImGui::IsMouseHoveringRect( r0, r1 );
    if ( hovered )
    {
      hoveredNode = i;
      draggedPos = posx;
    }
    nodeList.push_back( { posx, hovered } );

    if ( posx - halfWidth < cs.x * pos && posx + halfWidth > cs.x * pos )
    {
      nodeUnderCursor = i;
    }
  }

  if ( hoveredNode.has_value() && ImGui::IsMouseDragging( ImGuiMouseButton_Left ) )
  {
    model.updateKeyFrame( *hoveredNode, (int)( ( draggedPos + ImGui::GetMouseDragDelta( ImGuiMouseButton_Left ).x ) * maxFrame / cs.x ) );
  }

  if ( ImGui::IsMouseReleased( ImGuiMouseButton_Left ) )
  {
    hoveredNode.reset();
  }


  for ( size_t i = 0; i < nodeList.size(); ++i )
  {
    auto const& node = nodeList[i];

    ImU32 color = node.second ? IM_COL32( 100, 255, 200, 255 ) : IM_COL32( 255, 255, 200, 255 );

    draw_list->AddRectFilled( ImVec2( c0.x + node.first - halfWidth, c0.y + 1 ), ImVec2( c0.x + node.first + halfWidth, c0.y + cs.y - 1 ), color, 5.0f, ImDrawCornerFlags_All );
  }

  static bool keyFrameHover;
  static bool borderKeyFrameHover;
  static size_t nodeToDelete;


  ImVec2 dragDelta = ImGui::GetMouseDragDelta( ImGuiMouseButton_Right );
  if ( ImGui::IsMouseReleased( ImGuiMouseButton_Right ) && dragDelta.x == 0.0f && dragDelta.y == 0.0f )
  {
    keyFrameHover = hoveredNode.has_value();
    if ( keyFrameHover )
    {
      borderKeyFrameHover = ( *hoveredNode == 0 || *hoveredNode == nodeList.size() - 1 );
      nodeToDelete = *hoveredNode;
    }
    else
    {
      borderKeyFrameHover = false;
    }

    if ( !borderKeyFrameHover )
      ImGui::OpenPopupOnItemClick( "context" );
  }

  if ( !borderKeyFrameHover )
  {
    ImGui::PushStyleVar( ImGuiStyleVar_WindowPadding, ImVec2( 4, 4 ) );
    if ( ImGui::BeginPopup( "context" ) )
    {
      if ( keyFrameHover )
      {
        //!nodeList.front().second && !nodeList.back().second && 
        if ( ImGui::MenuItem( "Delete keyframe", NULL, false, true ) )
        {
          model.deleteKeyFrame( nodeToDelete );
        }
      }
      else
      {
        if ( ImGui::MenuItem( "Add keyframe @ cursor", NULL, false, true ) )
        {
          for ( size_t i = 0; i < nodeList.size() - 1; ++i )
          {
            if ( mousePos > nodeList[i].first && mousePos < nodeList[i + 1].first )
            {
              model.addKeyFrame( i + 1, pos );
              break;
            }
          }
        }
      }
      ImGui::EndPopup();
    }
    ImGui::PopStyleVar();
  }
}

void WinPreviewRenderer::drawOffset( IModel & model, std::optional<size_t> nodeUnderCursor )
{
  constexpr float DISTANCE = 30.0f;

  static std::optional<int> orgOffset;

  if ( !nodeUnderCursor.has_value() )
    return;

  ImGuiIO& io = ImGui::GetIO();
  ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;

  ImVec2 window_pos = ImVec2( io.DisplaySize.x - DISTANCE, io.DisplaySize.y / 2 );
  ImGui::SetNextWindowPos( window_pos, ImGuiCond_Appearing, ImVec2{ 0.5, 0.5 } );
  ImGui::SetNextWindowBgAlpha( 0.5f ); // Transparent background
  if ( ImGui::Begin( "Offset", nullptr, window_flags ) )
  {
    if ( ImGui::IsWindowHovered() )
    {
      if ( ImGui::IsMouseReleased( ImGuiMouseButton_Left ) )
      {
        orgOffset.reset();
      }

      if ( ImGui::IsMouseDragging( ImGuiMouseButton_Left ) )
      {
        if ( !orgOffset.has_value() )
          orgOffset = model.getData().keyFrames[*nodeUnderCursor].offset;

        int newOffset = *orgOffset - (int)ImGui::GetMouseDragDelta( ImGuiMouseButton_Left ).y;
        ImGui::Text( "%d", newOffset );
        model.updateKeyOffset( *nodeUnderCursor, newOffset );
      }
    }
  }
  ImGui::End();

}

void WinPreviewRenderer::renderGui( float pos, IModel & model )
{
  ImGuiIO& io = ImGui::GetIO();

  mImgui->dx11_NewFrame();
  mImgui->win32_NewFrame();

  ImGui::NewFrame();

  ImGui::SetNextWindowSizeConstraints( ImVec2( 800, 200 ), ImVec2( FLT_MAX, FLT_MAX ) );
  ImGui::PushStyleVar( ImGuiStyleVar_WindowPadding, ImVec2( 0, 0 ) );
  ImGui::PushStyleVar( ImGuiStyleVar_ItemSpacing, ImVec2( 0, 0 ) );
  ImGui::SetNextWindowBgAlpha( 0.5f );
  if ( ImGui::Begin( "Timeline" ) )
  {
    drawRuler( model, pos );
    std::optional<size_t> nodeUnderCursor{};
    drawKeyFrames( model, pos, nodeUnderCursor );
    drawEnvelopes( model, pos );
    drawOffset( model, nodeUnderCursor );
  }
  ImGui::End();
  ImGui::PopStyleVar();
  ImGui::PopStyleVar();

  ImGui::Render();

  mImmediateContext->OMSetRenderTargets( 1, &mBackBufferRTV.p, nullptr );
  mImgui->dx11_RenderDrawData( ImGui::GetDrawData() );

  std::array<ID3D11RenderTargetView *const, 1> rtv{};
  mImmediateContext->OMSetRenderTargets( 1, rtv.data(), nullptr );

  mSwapChain->Present( model.getEncoder() ? 0 : 1, 0 );
}
