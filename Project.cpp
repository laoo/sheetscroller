#include "Project.hpp"
#include <functional>
#include "Ex.hpp"
#include "pugixml/src/pugixml.hpp"

using namespace std::string_literals;

std::function<bool( pugi::xml_attribute const& )> atrFun( std::string const& str )
{
  return[str]( pugi::xml_attribute const& atr )
  {
    return atr.name() == str;
  };
}

std::function<bool( pugi::xml_attribute const& )> operator""_l( char const * str, size_t )
{
  return atrFun( str );
}


Project::Project( std::filesystem::path const & path ) : mImagePath{}, mAudioPath{}
{
  if ( !std::filesystem::exists( path ) )
  {
    throw Ex{} << "Project file " << path << " does not exist";
  }

  pugi::xml_document doc;
  pugi::xml_parse_result xresult = doc.load_file( path.string().c_str() );

  if ( !xresult )
  {
    throw Ex{} << "Error parsing " << path;
  }


  auto node = doc.root().first_child();

  if ( node.name() != "SheetScroller"s )
    throw Ex{} << "Project root node must be 'SheetScroller'";

  for ( auto child : node )
  {
    if ( child.name() == "Source"s )
    {
      mImagePath = child.find_attribute( "Image"_l ).as_string();
      mAudioPath = child.find_attribute( "Audio"_l ).as_string();
    }
    else if ( child.name() == "Effects"s )
    {
      mFadeIn = child.find_attribute( "FadeIn"_l ).as_int();
      mFadeOut = child.find_attribute( "FadeOut"_l ).as_int();
      mCrossFade = child.find_attribute( "CrossFade"_l ).as_int();
    }
    else if ( child.name() == "Destination"s )
    {
      mVideoPath = child.find_attribute( "Video"_l ).as_string();
      mVideoBitrate = child.find_attribute( "VideoBitrate"_l ).as_int();
      mAudioBitrate = child.find_attribute( "AudioBitrate"_l ).as_int();
    }
    else if ( child.name() == "KeyFrames"s )
    {
      for ( auto key : child )
      {
        if ( key.name() == "Key"s )
        {
          int offset = key.find_attribute( "Offset"_l ).as_int();
          int frame{};
          if ( auto tc = key.find_attribute( "Timecode"_l ) )
          {
            frame = parseTimecode( tc.as_string() + ":"s );
          }
          else if ( auto tc = key.find_attribute( "Seconds"_l ) )
          {
            frame = (int)( tc.as_float() * 60.0f );
          }
          else
          {
            throw Ex{} << "Key node must have either Timecode or Seconds attribute";
          }

          addKeyFrame( offset, frame );
        }
      }
    }
    else if ( child.name() == "Envelope"s )
    {
      for ( auto key : child )
      {
        if ( key.name() == "Node"s )
        {
          float value = key.find_attribute( "Value"_l ).as_float();
          int frame{};
          if ( auto tc = key.find_attribute( "Timecode"_l ) )
          {
            frame = parseTimecode( tc.as_string() + ":"s );
          }
          else if ( auto tc = key.find_attribute( "Seconds"_l ) )
          {
            frame = (int)( tc.as_float() * 60.0f );
          }
          else
          {
            throw Ex{} << "Node node must have either Timecode or Seconds attribute";
          }

          addNode( value, frame );
        }
      }
    }
    else
    {
      throw Ex{} << "Unknown project node " << child.name();
    }
  }
}

std::filesystem::path Project::imagePath() const
{
  return mImagePath;
}

std::filesystem::path Project::audioPath() const
{
  return mAudioPath;
}

std::filesystem::path Project::videoPath() const
{
  return mVideoPath;
}

int Project::videoBitrate() const
{
  return mVideoBitrate;
}

int Project::audioBitrate() const
{
  return mAudioBitrate;
}

int Project::fadeIn() const
{
  return mFadeIn;
}

int Project::fadeOut() const
{
  return mFadeOut;
}

int Project::crossFade() const
{
  return mCrossFade;
}

void Project::addKeyFrame( int offset, int frame )
{
  mKeyFrames.push_back( { offset, frame } );
}

void Project::addNode( float value, int frame )
{
  mNodes.push_back( { value, frame } );
}

int Project::parseTimecode( std::string const & tc )
{
  std::vector<int> parts;
  for ( size_t off = 0;; )
  {
    size_t limit = tc.find_first_of( ':', off );
    if ( limit != std::string::npos )
    {
      parts.push_back( atoi( tc.c_str() + off ) );
      off = limit + 1;
    }
    else
    {
      break;
    }
  }

  if ( parts.empty() )
    throw Ex{} << "Bad Key#Timecode";

  for ( int i = (int)parts.size() - 1; i > 0; --i )
  {
    for ( int j = 0; j < i; ++j )
    {
      parts[j] *= 60;
    }
  }

  int frames{};
  for ( auto p : parts )
  {
    frames += p;
  }

  return frames;
}
