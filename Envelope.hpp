#pragma once

#include "ScrollingData.hpp"
#include <vector>
#include <memory>

struct ScrollingData;

struct MappedPos
{
  float offset1;
  float offset2;
  float alpha;
};

class Envelope
{
public:
  Envelope( int frames, int fadeIn, int fadeOut, int crossFade, ScrollingData & data, std::shared_ptr<std::vector<float>> & bezierPoints );
  ~Envelope();

  MappedPos map( int idx ) const;

  static double bezier( double t, double w[4] );


private:
  static std::vector<double> computeFuntion( std::vector<ScrollingData::Node> & nodes, std::shared_ptr<std::vector<float>> & bez );

private:
  std::vector<ScrollingData::Key> mKeyFrames;
  std::vector<double> mEnvelope;
  int mFrames;
  int mFadeIn;
  int mFadeOut;
  int mCrossFade;
};
