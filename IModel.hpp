#pragma once

#include <memory>

struct ScrollingData;
class IEncoder;

class IModel
{
public:
  virtual ~IModel() = default;

  virtual ScrollingData const& getData() = 0;
  virtual std::shared_ptr<std::vector<float>> getBezierPoints() const = 0;
  virtual void addKeyFrame( size_t before, float pos ) = 0;
  virtual bool updateKeyFrame( size_t key, int frame ) = 0;
  virtual bool updateKeyOffset( size_t key, int offset ) = 0;
  virtual void deleteKeyFrame( size_t key ) = 0;
  
  virtual void addEnvNode( int frame, float value ) = 0;
  virtual void deleteEnvNode( size_t node ) = 0;
  virtual void updateEnvNode( size_t node, int frame, float value ) = 0;

  virtual void seek( float pos ) = 0;
  virtual bool previewFades() const = 0;

  virtual IEncoder * getEncoder() = 0;


};
