#include <AudioFile/AudioFile.h>
#include <Windows.h>
#include <array>
#include <string>
#include <stdexcept>
#include <fstream>
#include <cassert>
#include <sstream>
#include <cwchar>
#include <vector>
#include <sstream>
#include "Model.hpp"
#include "WinPreviewRenderer.hpp"
#include "WinAudioOut.hpp"
#include "ScrollingData.hpp"
#include "Ex.hpp"
#include "Project.hpp"
#include "Log.hpp"

wchar_t gClassName[] = L"SheetScrollerWindowClass";

extern LRESULT win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

std::vector<std::wstring> gDroppedFiles;


void handleFileDrop( HDROP hDrop )
{
#ifdef _WIN64
  auto h = GlobalAlloc( GMEM_MOVEABLE, 0 );
  uintptr_t hptr = reinterpret_cast<uintptr_t>( h );
  GlobalFree( h );
  uintptr_t hdropptr = reinterpret_cast<uintptr_t>( hDrop );
  hDrop = reinterpret_cast<HDROP>( hptr & 0xffffffff00000000 | hdropptr & 0xffffffff );
#endif

  uint32_t cnt = DragQueryFile( hDrop, ~0, nullptr, 0 );
  gDroppedFiles.resize( cnt );

  for ( uint32_t i = 0; i < cnt; ++i )
  {
    uint32_t size = DragQueryFile( hDrop, i, nullptr, 0 );
    gDroppedFiles[i].resize( size + 1 );
    DragQueryFile( hDrop, i, gDroppedFiles[i].data(), size + 1 );
  }

  DragFinish( hDrop );
}

LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{

  switch ( msg )
  {
  case WM_CREATE:
  {
    WinPreviewRenderer* pRenderer = reinterpret_cast<WinPreviewRenderer*>( reinterpret_cast<LPCREATESTRUCT>( lParam )->lpCreateParams );
    assert( pRenderer );
    try
    {
      pRenderer->initialize( hWnd );
    }
    catch ( std::exception const& ex )
    {
      MessageBoxA( nullptr, ex.what(), "SV20DTool Error", MB_OK | MB_ICONERROR );
      PostQuitMessage( 0 );
    }
    SetWindowLongPtr( hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>( pRenderer ) );
    return 0;
  }
  case WM_CLOSE:
    DestroyWindow( hWnd );
    break;
  case WM_DESTROY:
    SetWindowLong( hWnd, GWLP_USERDATA, NULL );
    PostQuitMessage( 0 );
    break;
  case WM_DROPFILES:
    handleFileDrop( (HDROP)wParam );
  default:
    if ( WinPreviewRenderer* pRenderer = reinterpret_cast<WinPreviewRenderer*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) ) )
    {
      if ( pRenderer->win32_WndProcHandler( hWnd, msg, wParam, lParam ) )
        return true;
    }
    return DefWindowProc( hWnd, msg, wParam, lParam );
  }
  return 0;
}

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
  MSG msg{};

  L_SET_LOGLEVEL( ::Log::LL_DEBUG );

  try
  {

    std::vector<std::wstring> args;

    LPWSTR *szArgList;
    int argCount;

    szArgList = CommandLineToArgvW( GetCommandLine(), &argCount );
    if ( szArgList != NULL )
    {
      for ( int i = 1; i < argCount; i++ )
      {
        args.emplace_back( szArgList[i] );
      }

      LocalFree( szArgList );
    }

    if ( args.size() != 1 )
      throw Ex{} << "usage: SheetScroller Project.xml";

    WNDCLASSEX wc{ sizeof( WNDCLASSEX ), 0, WndProc, 0, 0, hInstance, LoadIcon( nullptr, IDI_APPLICATION ), LoadCursor( nullptr, IDC_ARROW ), (HBRUSH)( COLOR_WINDOW + 1 ), nullptr, gClassName, LoadIcon( nullptr, IDI_APPLICATION ) };

    if ( !RegisterClassEx( &wc ) )
    {
      return 0;
    }

    Project project{ args[0] };

    std::wstring name = L"SheetScroller " + args[0];

    AudioFile<float> audioFile;

    if ( !audioFile.load( project.audioPath().string() ) )
      throw Ex{} << "Error reading audio file " << project.audioPath();

    int videoFrames = (int)( audioFile.getLengthInSeconds() * 60.0 );

    WinPreviewRenderer renderer{ project, videoFrames };
    Model proc{ audioFile, project };
    WinAudioOut audio{ audioFile };

    HWND hwnd = CreateWindowEx( WS_EX_CLIENTEDGE, gClassName, name.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 1942, 1125, nullptr, nullptr, hInstance, reinterpret_cast<LPVOID>( &renderer ) );

    if ( hwnd == nullptr )
    {
      return 0;
    }

    ShowWindow( hwnd, nCmdShow );
    UpdateWindow( hwnd );

    int frame = 0;
    for ( ;; )
    {
      proc.resetLoop();

      while ( PeekMessage( &msg, nullptr, 0, 0, PM_REMOVE ) )
      {
        TranslateMessage( &msg );
        DispatchMessage( &msg );

        if ( msg.message == WM_QUIT )
          break;
      }

      if ( msg.message == WM_QUIT )
        break;

      if ( !gDroppedFiles.empty() )
      {
        for ( auto const& arg : gDroppedFiles )
        {
          //
        }

        gDroppedFiles.clear();
      }


      renderer.renderPreview( proc, proc.map( frame ) );
      renderer.renderGui( (float)frame / videoFrames, proc );

      audio.sync( proc.playing(), proc.isSeek() );

      if ( auto pos = proc.isSeek() )
      {
        frame = std::clamp( (int)( pos.value() * videoFrames ), 0, videoFrames - 1 );
      }
      else if ( proc.playing() || proc.mode() == Model::Mode::RENDERING )
      {
        frame = ( std::min )( frame + 1, videoFrames );
        if ( frame == videoFrames && proc.mode() == Model::Mode::RENDERING )
        {
          proc.resetPreview();
        }
      }
    }
  }
  catch ( std::exception const& ex )
  {
    MessageBoxA( nullptr, ex.what(), "SheetScroller Error", MB_OK | MB_ICONERROR );
    return -1;
  }

  return (int)msg.wParam;

}

