R"(
Texture2DArray<unorm float4> src : register( t0 );
RWTexture2D<unorm float4> dst : register( u0 );
RWTexture2D<unorm float> dstY : register( u1 );
RWTexture2D<unorm float> dstU : register( u2 );
RWTexture2D<unorm float> dstV : register( u3 );

cbuffer cb : register( b0 )
{
  float2 srcOff;
  uint2 _1;
  uint2 _2;
  float alpha;
  float alpha2;
};

float4 rgb2yuv(float4 rgbA, float4 rgbB)
{
  float4 rgb = float4( lerp( rgbA.rgb, rgbB.rgb, alpha ), 1 );
  float y = dot( rgb, float4( 0.257, 0.504, 0.098, 0.0625 ) );
  float u = dot( rgb, float4( -0.148, -0.291, 0.439, 0.5 ) );
  float v = dot( rgb, float4( 0.439, -0.368, -0.071, 0.5 ) );
  return float4(y,u,v,1);
}

float4 interpolate( int2 t )
{
  int2 t0 = t.xy - int2(0,1);
  int2 t1 = t.xy + int2(0,0);
  int2 t2 = t.xy + int2(0,1);
  int2 t3 = t.xy - int2(0,2);

  int3 tt0 = int3( t0.x, t0.y % 16384, t0.y / 16384 );
  int3 tt1 = int3( t1.x, t1.y % 16384, t1.y / 16384 );
  int3 tt2 = int3( t2.x, t2.y % 16384, t2.y / 16384 );
  int3 tt3 = int3( t3.x, t3.y % 16384, t3.y / 16384 );

  float4 c0 = src[tt0];
  float4 c1 = src[tt1];
  float4 c2 = src[tt2];
  float4 c3 = src[tt3];

  float mu = frac(srcOff.y);
  float mu2 = mu*mu;
  float4 a0 = c3 - c2 - c0 + c1;
  float4 a1 = c0 - c1 - a0;
  float4 a2 = c2 - c0;
  float4 a3 = c1;

  return a0*mu*mu2+a1*mu2+a2*mu+a3;
}


[numthreads( 128, 8, 1 )]
void main( uint3 DT : SV_DispatchThreadID, uint3 G : SV_GroupID, uint3 GT : SV_GroupThreadID )
{
  float4 c00A = interpolate( int2(0, trunc(srcOff.x)) + DT.xy * 2 );
  float4 c01A = interpolate( int2(0, trunc(srcOff.x)) + DT.xy * 2 + int2( 0, 1 ) );
  float4 c10A = interpolate( int2(0, trunc(srcOff.x)) + DT.xy * 2 + int2( 1, 0 ) );
  float4 c11A = interpolate( int2(0, trunc(srcOff.x)) + DT.xy * 2 + int2( 1, 1 ) );

  float4 c00B = interpolate( int2(0, trunc(srcOff.y)) + DT.xy * 2 );
  float4 c01B = interpolate( int2(0, trunc(srcOff.y)) + DT.xy * 2 + int2( 0, 1 ) );
  float4 c10B = interpolate( int2(0, trunc(srcOff.y)) + DT.xy * 2 + int2( 1, 0 ) );
  float4 c11B = interpolate( int2(0, trunc(srcOff.y)) + DT.xy * 2 + int2( 1, 1 ) );

  float4 yuv00 = rgb2yuv( c00A, c00B );  //asume a=1
  float4 yuv01 = rgb2yuv( c01A, c01B );  //asume a=1
  float4 yuv10 = rgb2yuv( c10A, c10B );  //asume a=1
  float4 yuv11 = rgb2yuv( c11A, c11B );  //asume a=1

  dst[DT.xy * 2] = lerp( c00A, c00B, alpha2 );
  dst[DT.xy * 2 + int2( 0, 1 )] = lerp( c01A, c01B, alpha2 );
  dst[DT.xy * 2 + int2( 1, 0 )] = lerp( c10A, c10B, alpha2 );
  dst[DT.xy * 2 + int2( 1, 1 )] = lerp( c11A, c11B, alpha2 );

  dstY[DT.xy * 2] = yuv00.x;
  dstY[DT.xy * 2 + int2( 0, 1 )] = yuv01.x;
  dstY[DT.xy * 2 + int2( 1, 0 )] = yuv10.x;
  dstY[DT.xy * 2 + int2( 1, 1 )] = yuv11.x;

  float4 uv = ( yuv00 + yuv01 + yuv10 + yuv11 ) / 4;

  dstU[DT.xy] = uv.y;
  dstV[DT.xy] = uv.z;
}
)";
